CREATE TABLE IF NOT EXISTS default.stock_prices (
    price_time DateTime,
    ticker_name LowCardinality(String),
    price Decimal32(4) CODEC(DoubleDelta)
) ENGINE=MergeTree() ORDER BY (ticker_name, price_time) PARTITION BY toYYYYMM(price_time);