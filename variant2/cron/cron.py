from typing import Dict
import settings
import requests
import time
import io
import csv
import random
import logging
from http.client import HTTPConnection

class Ticker:
    def __init__(self):
        self.stock_prices: Dict = {}
        for i in range(settings.TICKER_COUNT):
            self.stock_prices[f"{settings.TICKER_NAME_PREFIX}_{i}"] = 0

    # @TODO could price be less than zero?
    def change_price(self) -> None:
        for i in range(settings.TICKER_COUNT):
            self.stock_prices[f"{settings.TICKER_NAME_PREFIX}_{i}"] += Ticker.generate_movement()

    @staticmethod
    def generate_movement() -> int:
        movement = -1 if random.random() < 0.5 else 1
        return movement

    def make_request_body(self, csv_writer):
        timestamp = int(time.time())
        for ticker_name, stock_price in self.stock_prices.items():
            csv_writer.writerow([timestamp, ticker_name, stock_price])


if __name__ == '__main__':
    logging.basicConfig(format="%(asctime)s:%(name)s [%(levelname)s]  %(message)s'", level=logging.INFO)
    logging.info("cron script ready for %s", settings.CLICKHOUSE_URL)

    # logging.getLogger().setLevel(logging.DEBUG)
    # HTTPConnection.debuglevel = 1
    # requests_log = logging.getLogger("requests.packages.urllib3")
    # requests_log.setLevel(logging.DEBUG)
    # requests_log.propagate = True

    ticker = Ticker()
    while True:
        time.sleep(settings.TICK_TIME_SECONDS)
        ticker.change_price()

        stream = io.StringIO()
        writer = csv.writer(stream, quoting=csv.QUOTE_NONNUMERIC)
        ticker.make_request_body(writer)

        try:
            resp = requests.post(settings.CLICKHOUSE_URL, params={"query": "INSERT INTO default.stock_prices FORMAT CSV", "async_insert": 1, "wait_for_async_insert": 1}, data=stream.getvalue())
            logging.info("write %d tickers %s", len(ticker.stock_prices), settings.CLICKHOUSE_URL)
        except Exception:
            logging.exception("requests error")
