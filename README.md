# README #

Test project for https://docs.google.com/document/d/1ru_fwWACv7Y_rVPZJyrXBpxvbU1Wdo8oRJQpl5jm1Uw/edit

## How do I get set up? ##
git clone git@bitbucket.org:bloodjazman/ticker_test.git

### Variant 1 - frontend Svelte.js / fastAPI backend + websocket ###

```bash
cd ./variant1/
docker-compose up --build -d
# open http://localhost:8000/
# select ticker__0 ticker__1 in drop-down 
```

### Variant 2 - Grafana + clickhouse + fastAPI ###

```bash
cd ./variant2/
docker-compose up --build -d
# open http://localhost:3000/
```


### Who do I talk to? ###

* Ask anything on https://t.me/bloodjazman
