import os
TICK_TIME_SECONDS = 1 if not os.getenv("TICK_TIME_SECONDS") else os.getenv("TICK_TIME_SECONDS")
TICKER_COUNT = 100 if not os.getenv("TICKER_COUNT") else os.getenv("TICKER_COUNT")
TICKER_NAME_PREFIX = "ticker_" if not os.getenv("TICKER_NAME_PREFIX") else os.getenv("TICKER_NAME_PREFIX")
