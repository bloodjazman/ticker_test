from typing import List, Dict
from fastapi import FastAPI, WebSocket, WebSocketDisconnect
from fastapi_utils.tasks import repeat_every
import random
import json
import time
import settings
import logging

logging.basicConfig(format="%(asctime)s:%(name)s [%(levelname)s]  %(message)s'")


class ConnectionManager:
    def __init__(self):
        self.active_connections: List[WebSocket] = []

    async def connect(self, websocket: WebSocket):
        await websocket.accept()
        self.active_connections.append(websocket)

    def disconnect(self, websocket: WebSocket):
        self.active_connections.remove(websocket)

    async def broadcast(self, message: str):
        logging.debug("broadcast %s to %d client", message, len(self.active_connections))
        for connection in self.active_connections:
            await connection.send_text(message)


class Ticker:
    def __init__(self):
        self.stock_prices: Dict = {}
        for i in range(settings.TICKER_COUNT):
            self.stock_prices[f"{settings.TICKER_NAME_PREFIX}_{i}"] = 0

    # @TODO could price be less than zero?
    def change_price(self) -> None:
        for i in range(settings.TICKER_COUNT):
            self.stock_prices[f"{settings.TICKER_NAME_PREFIX}_{i}"] += Ticker.generate_movement()

    @staticmethod
    def generate_movement() -> int:
        movement = -1 if random.random() < 0.5 else 1
        return movement


app = FastAPI()
connection_manager = ConnectionManager()
ticker = Ticker()


@app.on_event("startup")
@repeat_every(seconds=settings.TICK_TIME_SECONDS)
async def tick() -> None:
    ticker.change_price()
    await connection_manager.broadcast(json.dumps({"timestamp": int(time.time()), "stock_prices": ticker.stock_prices}))


@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await connection_manager.connect(websocket)
    try:
        while True:
            data = await websocket.receive_text()
            logging.warning(f"received from client: {data}")
    except WebSocketDisconnect:
        connection_manager.disconnect(websocket)
